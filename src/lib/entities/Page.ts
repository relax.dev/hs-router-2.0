import { PANEL_MAIN, VIEW_MAIN, ROOT_MAIN } from '../const';
import { RouterGenericBase } from './Types';

export class Page<T extends RouterGenericBase = RouterGenericBase> {
  public panelId: T['panelId'];
  public viewId: T['viewId'];
  public rootId: T['rootId'];
  public isInfinityPanel = false;

  constructor(panelId?: T['panelId'], viewId?: T['viewId'], rootId?: T['rootId']) {
    this.panelId = panelId || PANEL_MAIN;
    this.viewId = viewId || VIEW_MAIN;
    this.rootId = rootId || ROOT_MAIN;
  }

  clone() {
    const p = new Page(this.panelId, this.viewId, this.rootId);
    p.isInfinityPanel = this.isInfinityPanel;
    return p;
  }

  makeInfinity() {
    this.isInfinityPanel = true;
    return this;
  }
}
