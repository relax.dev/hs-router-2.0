import { Route } from './Route';
import { State } from './State';
import { InfinityPanelIdTitle, PageParams, RouterGenericBase } from './Types';

export class Location<T extends RouterGenericBase = RouterGenericBase> {
  route: Route<T>;
  state: State<T>;

  constructor(route: Route<T>, state: State<T>) {
    this.route = route;
    this.state = state;
  }

  /**
   * @ignore
   * @param viewId
   */
  public getLastPanelInView(viewId: T['viewId']): T['panelId'] | undefined {
    const state = this.state;
    if (state && state.panelInView[viewId]) {
      return state.panelInView[viewId];
    }
    return undefined;
  }

  /**
   * Массив из id панелей для передачи в атрибут history <View>
   *
   * ```javascript
   * import { useLocation } from '@happysanta/router';
   *
   * const App = () => {
   *    const location = useLocation();
   *    return <View id={VIEW_MAIN}
   *                 history={location.getViewHistory(VIEW_MAIN)}
   *                 activePanel={location.getViewActivePanel(VIEW_MAIN)}>
   *           <Home id={PANEL_MAIN}/>
   *           <Persik id={PANEL_PERSIK}/>
   *    </View>
   * }
   * ```
   *
   * @param viewId
   */
  public getViewHistory(viewId: T['viewId']): Array<T['panelId'] | InfinityPanelIdTitle<T['panelId']>> {
    const route = this.route;
    const state = this.state;
    if (route.getViewId() === viewId) {
      return state.history;
    } else {
      const lastPanelId = this.getViewActivePanel(viewId);
      if (lastPanelId) {
        return [lastPanelId];
      }
      return [];
    }
  }

  public getViewHistoryWithLastPanel(viewId: T['viewId']): Array<T['panelId'] | InfinityPanelIdTitle<T['panelId']>> {
    const history = this.getViewHistory(viewId);
    const lastPanel = this.getLastPanelInView(viewId);
    if (lastPanel && !history.includes(lastPanel)) {
      return history.concat([lastPanel]);
    } else {
      return history;
    }
  }

  /**
   * @deprecated use getViewActivePanel
   * @ignore
   * @param viewId
   */
  public getPanelIdInView(viewId: T['viewId']): T['panelId'] | InfinityPanelIdTitle<T['panelId']> | undefined {
    return this.getViewActivePanel(viewId);
  }

  public getViewActivePanel(viewId: T['viewId']): T['panelId'] | InfinityPanelIdTitle<T['panelId']> | undefined {
    const route = this.route;
    if (route.getViewId() === viewId) {
      return route.getPanelId();
    } else {
      return this.getLastPanelInView(viewId);
    }
  }

  public getPanelId(): T['panelId'] | InfinityPanelIdTitle<T['panelId']> {
    return this.route.getPanelId();
  }

  public getViewId(): T['viewId'] {
    return this.route.getViewId();
  }

  public getRootId(): T['rootId'] {
    return this.route.getRootId();
  }

  public getModalId(): T['modalId'] | null {
    return this.route.getModalId();
  }

  public getPopupId(): T['popupId'] | null {
    return this.route.getPopupId();
  }

  public getPageId(): T['pageId'] {
    return this.route.getPageId();
  }

  public getParams(): PageParams {
    return this.route.getParams();
  }

  public hasOverlay() {
    return this.route.hasOverlay();
  }

  /**
   * Если вам надо отрисовать стрелочку назад или домик то используйте эту функцию
   */
  public isFirstPage(): boolean {
    return this.state.first === 1;
  }
}
