import { RouterMiddleware } from './Router';
import { UsedFixers } from './HotFixers';
import { RouterGenericBase } from './Types';

export interface RouterConfig<T extends RouterGenericBase = RouterGenericBase> {
  /**
   * Добавить вывод логов для отладки переходов
   */
  enableLogging?: boolean;
  defaultPage?: T['pageId'];
  defaultView?: T['viewId'];
  defaultPanel?: T['panelId'];
  /**
   * всегда используйте `true` чтобы избежать путаницы с адресами vk.com/app123#product/123 и vk.com/app123#/product/123
   */
  noSlash?: boolean;
  blankMiddleware?: Array<RouterMiddleware<T>>;
  preventSameLocationChange?: boolean;

  hotFixes?: UsedFixers;
}
