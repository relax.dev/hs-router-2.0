import React, { PropsWithChildren } from 'react';
import { Router } from './Router';
import { RouterGenericBase } from './Types';

/**
 * Все приложение необходимо оборачивать в контекст для корректной работы {@link withRouter} {@link useRouter}
 *
 * ```javascript
 * import { RouterContext } from '@happysanta/router';
 * import { router } from './routers';
 *
 * ReactDOM.render(<RouterContext.Provider value={router}>
 *   <ConfigProvider isWebView={true}>
 *     <App/>
 *   </ConfigProvider>
 * </RouterContext.Provider>, document.getElementById('root'));
 * ```
 */
export const RouterContext = React.createContext<Router<any> | null>(null);
RouterContext.displayName = 'Router';

type Props<T extends RouterGenericBase = RouterGenericBase> = PropsWithChildren<{
  router: Router<T>;
}>;

export const RouterProvider = <T extends RouterGenericBase = RouterGenericBase>({ children, router }: Props<T>) => {
  return <RouterContext.Provider value={router}>{children}</RouterContext.Provider>;
};
