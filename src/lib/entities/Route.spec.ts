import { Page } from './Page';
import { Route } from './Route';
import { RouteList } from './Router';

const mockPage = new Page('mock-panel', 'mock-view');

test('from location', () => {
  const list: RouteList = {
    '/': mockPage,
  };
  {
    const location = '';
    expect(!!Route.fromLocation(list, location)).toBe(true);
    expect(!!Route.fromLocation(list, location, true)).toBe(true);
  }
  {
    const location = '/';
    expect(!!Route.fromLocation(list, location)).toBe(true);
    expect(!!Route.fromLocation(list, location, true)).toBe(true);
  }
  {
    const location = '/?utm_source=ad';
    expect(!!Route.fromLocation(list, location)).toBe(true);
    expect(!!Route.fromLocation(list, location, true)).toBe(true);
  }
});

test('route clone', () => {
  const page = new Page('vew_main', 'panel_main').makeInfinity();
  const route = new Route(page, '/', { id: '15' });

  expect(JSON.stringify(route)).toEqual(JSON.stringify(route.clone()));
});

it('should return location', () => {
  const route1 = new Route(mockPage, '/', { mockParam: 'mock-value' });
  const route2 = new Route(mockPage, '/mock-page', {});
  expect(route1.location).toBe('#?mockParam=mock-value');
  expect(route2.location).toBe('#mock-page');
});
