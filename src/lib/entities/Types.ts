/**
 * Параметры страницы {@link Router.pushPage} {@link pushPage}
 */
export declare type PageParams = { [key: string]: string };
/**
 * @ignore
 */
export declare type HistoryUpdateType = 'PUSH' | 'REPLACE' | 'MOVE';

export declare type InfinityPanelIdTitle<PanelIdType extends string = string> = `_${PanelIdType}..${number}`;

export declare interface RouterGenericBase {
  panelId: string;
  viewId: string;
  modalId: string;
  pageId: string;
  popupId: string;
  rootId: string;
}
