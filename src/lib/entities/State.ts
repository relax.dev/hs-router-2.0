/**
 * @ignore
 * @packageDocumentation
 */

import { InfinityPanelIdTitle, RouterGenericBase } from './Types';

/**
 * @ignore
 */
export interface State<T extends RouterGenericBase = RouterGenericBase> {
  i: string;
  history: Array<T['panelId'] | InfinityPanelIdTitle<T['panelId']>>;
  panelInView: Partial<Record<T['viewId'], T['panelId']>>;
  length: number; // window.history.length;
  index: number; // ourIndex;
  blank: 0 | 1;
  first: 0 | 1;
}

let randomIdForCheckState = `${Math.random() * 2000000}.${Date.now()}`;

/**
 * Используется для тестов где не сбрасывается состояние jsdom
 * @ignore
 */
export function __testResetHistoryUniqueId() {
  randomIdForCheckState = `${Math.random() * 2000000}.${Date.now()}`;
}

/**
 * @ignore
 * @param currentIndex
 */
export function stateFromLocation<T extends RouterGenericBase = RouterGenericBase>(currentIndex: number): State<T> {
  const state = window.history.state;
  if (state && typeof state === 'object') {
    const s = state;
    if (s.i === randomIdForCheckState) {
      return { ...s };
    }
  }
  return {
    blank: 1,
    first: 0,
    length: window.history.length,
    index: currentIndex,
    history: [],
    i: randomIdForCheckState,
    panelInView: {},
  };
}
