/**
 * @ignore
 * @packageDocumentation
 */

import { RouterGenericBase } from '../entities/Types';
import { useFirstPageCheck } from './useFirstPageCheck';

/**
 * @deprecated use useFirstPageCheck
 * @ignore
 */
export function useHomePageCheck<T extends RouterGenericBase = RouterGenericBase>(): boolean {
  return useFirstPageCheck<T>();
}
