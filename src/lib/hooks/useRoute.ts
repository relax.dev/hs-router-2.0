/**
 * @ignore
 * @packageDocumentation
 */

import { Route, useLocation } from '../..';
import { RouterGenericBase } from '../entities/Types';

/**
 * @param withUpdate
 * @param panelId
 * @deprecated useRouter
 * @ignore
 */
export function useRoute<T extends RouterGenericBase = RouterGenericBase>(
  withUpdate = true,
  panelId?: T['panelId']
): Route<T> {
  const location = useLocation<T>(withUpdate, panelId);
  return location.route;
}
