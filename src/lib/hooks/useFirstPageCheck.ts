import { RouterGenericBase } from '../entities/Types';
import { useLocation } from './useLocation';

/**
 * Проверка на первую страницу для ситуаций когда пользователь входит в приложение по ссылке вида
 * https://vk.com/app7574523#product/12
 * @param withUpdate
 * @param panelId
 */

export function useFirstPageCheck<T extends RouterGenericBase = RouterGenericBase>(
  withUpdate = false,
  panelId?: T['panelId']
): boolean {
  const location = useLocation<T>(withUpdate, panelId);
  return location.isFirstPage();
}
