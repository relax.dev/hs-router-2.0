import { useContext, useEffect, useState } from 'react';
import { Router, RouterContext } from '../..';
import { RouterGenericBase } from '../entities/Types';

const useForceUpdate = () => useState<number>(0)[1];

export function useRouter<T extends RouterGenericBase = RouterGenericBase>(withUpdate = false): Router<T> {
  const router = useContext(RouterContext) as Router<T> | null;
  if (!router) {
    throw new Error('Use useRoute without context');
  }
  const forceUpdate = useForceUpdate();
  useEffect(() => {
    const fn = () => {
      if (withUpdate) {
        forceUpdate(Date.now());
      }
    };
    router.on('update', fn);
    return () => {
      router.off('update', fn);
    };
  }, []);
  return router;
}
