import { Location, useRouter } from '../..';
import { useRef } from 'react';
import { RouterGenericBase } from '../entities/Types';

/**
 * @param withUpdate
 * @param {string} panelId id панели для которой надо получить Location
 */
export function useLocation<T extends RouterGenericBase = RouterGenericBase>(
  withUpdate = true,
  panelId?: T['panelId']
): Location<T> {
  const router = useRouter<T>(withUpdate);
  const cachedLocation = useRef(router.getCurrentLocation());
  const prevLocation = useRef(router.getPreviousLocation());
  if (withUpdate) {
    const curLocation = router.getCurrentLocation();
    const prevLocation = router.getPreviousLocation();
    if (panelId && prevLocation?.getPanelId() === panelId) {
      return prevLocation;
    }
    return curLocation;
  } else {
    if (panelId && prevLocation.current?.getPanelId() === panelId) {
      return prevLocation.current;
    }
    return cachedLocation.current;
  }
}
