import { PageParams, useLocation } from '../..';
import { useRef } from 'react';
import { RouterGenericBase } from '../entities/Types';

/**
 * Возвращает {@link PageParams} текущего {@link Location}
 * если передать panelId то можно получить правильные параметры для "предыдущей" панели во время жеста swipe back
 * https://github.com/HappySanta/router/issues/16
 * @param {string} panelId id панели для которой надо получить параметры
 */
export function useParams<T extends RouterGenericBase = RouterGenericBase>(panelId?: T['panelId']): PageParams {
  const location = useLocation<T>(false, panelId);
  const params = useRef(location.getParams());
  return params.current;
}
